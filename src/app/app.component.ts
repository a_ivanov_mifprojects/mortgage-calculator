import {Component} from '@angular/core';
import {OverlayContainer} from '@angular/cdk/overlay';

@Component({
  selector: 'corsac-calc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  percentage = [0.3, 0.4, 0.45, 0.6, 0.9];

  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('theme');
  }
}
