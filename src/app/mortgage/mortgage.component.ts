import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'mortgage-calculator-mortgage',
  templateUrl: './mortgage.component.html',
  styleUrls: ['./mortgage.component.scss']
})
export class MortgageComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  private _percentage: number[];

  get percentage(): number[] {
    return this._percentage;
  }

  @Input()
  set percentage(value: number[]) {
    this._percentage = value;
  }

  terms = [1, 2, 3, 4, 5];
  rateTypes = ['Fixed', 'Variable', 'Fixed Open', 'Variable Open'];
  buyingForm: FormGroup;
  mortgageLoanAmount: number;
  renewingForm: FormGroup;
  paymentFrequency = ['Monthly', 'Semi-monthly', 'Bi-weekly', 'Accelerated Bi-weekly'];
  remainingAmortization = [5, 10, 15, 20, 25, 30];
  refinancingForm: FormGroup;
  cashReward: number;

  constructor(private formBuilder: FormBuilder) {
    this.buyingForm = this.formBuilder.group({
      price: ['', [Validators.required, Validators.min(1)]],
      downPayment: [{value: '', disabled: true}, Validators.min(1)],
      term: ['', [Validators.required]],
      rateType: ['', [Validators.required]],
    });

    this.buyingForm.get('downPayment').setValidators([Validators.required, this.checkDownPayment(this.buyingForm.get('price'))]);

    this.renewingForm = this.formBuilder.group({
      mortgageBalance: ['', [Validators.required, Validators.min(1)]],
      remainingAmortization: ['', [Validators.required]],
      term: ['', [Validators.required]],
      rateType: ['', [Validators.required]],
      paymentFrequency: ['', [Validators.required]],
    });
    this.refinancingForm = this.formBuilder.group({
      homeValue: ['', [Validators.required, Validators.min(1)]],
      mortgageBalance: ['', [Validators.required, Validators.min(1)]],
      amountAvailable: [{value: '', disabled: true}, [Validators.required]],
      term: ['', [Validators.required]],
      rateType: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.listenPrice();
    this.listenRefinancingHomeValue();
  }

  listenPrice() {
    this.buyingForm.get('price').valueChanges.subscribe(() => this.buyingForm.get('price').valid ?
      this.buyingForm.get('downPayment').enable() : this.buyingForm.get('downPayment').disable());
  }

  checkDownPayment(price: AbstractControl) {
    return (control: AbstractControl) => {
      if (this.buyingForm.value.price <= control.value) {
        return {downPayment: true};
      }
      return null;
    };
  }

  clearData() {
    const forms = [this.buyingForm, this.renewingForm, this.refinancingForm];

    forms.forEach(form => {
      form.reset();
      form.markAsPristine();
      form.markAsUntouched();
    });
    this.cashReward = null;
    this.mortgageLoanAmount = null;
  }

  buyingMortgageCalc({value}) {
    const mortgage = ((value.price - value.downPayment) * (this.percentage[value.term - 1])) / 100;
    if (mortgage < 0) {
      this.cashReward = null;
      return;
    }
    this.cashReward = Math.round(100 * mortgage) / 100;
  }

  renewingMortgageCalc({value}) {
    const mortgage = (value.mortgageBalance * (this.percentage[value.term - 1])) / 100;
    this.cashReward = Math.round(100 * mortgage) / 100;
  }

  listenRefinancingHomeValue() {
    this.refinancingForm.get('homeValue').valueChanges.subscribe(value => {
      this.refinancingForm.get('homeValue').valid ?
        this.refinancingForm.get('amountAvailable').setValue(value * 0.8) :
        this.refinancingForm.get('amountAvailable').setValue(null);
    });
  }

  refinancingMortgageCalc({value}) {
    const mortgage = (value.homeValue * 0.8 * (this.percentage[value.term - 1])) / 100;
    this.cashReward = Math.round(100 * mortgage) / 100;
  }

  setYear(year: number) {
    if (year > 1) {
      return 'years';
    }
    return 'year';
  }
}
